import http from 'http';
import './index.css';

let app = require('./server').default;

const server = http.createServer(app);

let currentApp = app;
console.log(process.env.PORT);
server
  .listen(process.env.PORT, () => {
    console.log('🚀 started');
  })
  .on('error', error => {
    console.log(error);
  });

if (module.hot) {
  console.log('✅  Server-side HMR Enabled!');

  module.hot.accept('./server', () => {
    console.log('🔁  HMR Reloading `./server`...');

    try {
      app = require('./server').default;
      server.removeListener('request', currentApp);
      server.on('request', app);
      currentApp = app;
    } catch (error) {
      console.error(error);
    }
  });
}
