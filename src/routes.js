
import React, { Component } from 'react';
import { StaticRouter, Route, Switch, Router} from 'react-router-dom';
import MainContent from './components/MainContent';

class Routes extends Component {
  render() {
    return (
        <Switch>
            <Route exact path="/" component={MainContent} />
        </Switch>     
    );
  }
}

export default Routes;
