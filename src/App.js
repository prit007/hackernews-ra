import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Home from './Home';
import './App.css';
import TopMenu from './components/TopMenu';
import Routes from './routes';
import MainContent from './components/MainContent';

function App() {
  return (
    <div className="App">      
      <TopMenu />
      <Routes />
    </div>
  );
}

export default App;

