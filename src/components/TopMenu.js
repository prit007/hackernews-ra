import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Face';
import { MenuItem } from '@material-ui/core';
import { NAV_LINKS } from '../constants/URLConstants';


const useStyles = makeStyles(theme => ({
  appBar: {
  },
  toolBar: {
    minHeight: '34px'
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    fontSize: '0.9rem'
  },
}));

function TopMenu() {
  const classes = useStyles();

  return (
    <AppBar position='fixed' className={classes.appBar}>
      <Toolbar className={classes.toolBar}>        
        {NAV_LINKS && NAV_LINKS.map((data, index) =>
                <MenuItem key={index}>
                <Typography variant='h6' className={classes.title}>
                {data.link}
                </Typography>
                </MenuItem>
        )}       
      
      </Toolbar>
    </AppBar>
  );
}

export default TopMenu;