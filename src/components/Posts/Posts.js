import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import './Posts.css';
import { deepOrange } from '@material-ui/core/colors';
import Skeleton from '@material-ui/lab/Skeleton';
import LineCharts from "./LineChart";

const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%'      
    },
    inline: {
      display: 'inline',
      fontSize: '13px',
      margin: '0px'
    },
    postsParnetBlock: {
        width: '100%',
        display: 'flex',
        backgroundColor: theme.palette.background.paper
    },
    primaryListItemLink: {
        cursor: 'pointer'
    },
    postsLists: {
        flexFlow: 'row wrap'
    },
    square: {
        color: theme.palette.getContrastText(deepOrange[500]),
        backgroundColor: deepOrange[500],
    },
    parentSkeleton: {
      display: 'flex',
      paddingBottom: '30px'
    },
    childSkeletonBlock : {
      flexGrow: 4,
      paddingLeft: '15px',
      paddingRight: '18px'
    },
    postsBlock : {
      width: '60%'
    },
    chartsBlock: {
      width: '40%'
    }
}));
  

function Posts({ posts, page, rowsPerPage, hiddenIds }) {
    let browserSessionStorage;
    if(typeof(window) !== 'undefined') {
        browserSessionStorage = window.sessionStorage;
    }
    const classes = useStyles();
    const [upvoteText, setUpvoteText] = React.useState('upvote');
    const [upvoteIds, setUpvoteId] = React.useState([]);
    const handleListItemClick = (event, data, index) => {
      if (typeof window !== 'undefined') {
        window.open(data.url, "_blank");
      }       
    };
    const hide = (event, data) => {
      if (browserSessionStorage) {
        let getHiddenValues = JSON.parse(browserSessionStorage.getItem('hide'))  && JSON.parse(browserSessionStorage.getItem('hide')).length > 0 ? JSON.parse(browserSessionStorage.getItem('hide')) : [];
        if (getHiddenValues.indexOf(data.id) === -1) {
            getHiddenValues.push(data.id);
            browserSessionStorage.setItem('hide', JSON.stringify(getHiddenValues));
            hiddenIds(getHiddenValues);
        } 
      }          
    };

    const upvote = (event, data) => {
      if (browserSessionStorage) {
        let getUpvoteValues = JSON.parse(browserSessionStorage.getItem('upvote'))  && JSON.parse(browserSessionStorage.getItem('upvote')).length > 0 ? JSON.parse(browserSessionStorage.getItem('upvote')) : [];
        if (getUpvoteValues.findIndex(val => val.id === data.id) === -1 && upvoteIds && upvoteIds.findIndex(val => val.id === data.id) === -1) {
            getUpvoteValues.push({id: data.id, upvote:1});
            browserSessionStorage.setItem('upvote', JSON.stringify(getUpvoteValues));
            setUpvoteId(prevState => [...prevState, {id: data.id, upvote:1}]);
        } 
        else {
            let copyUpVoteIdState = [...upvoteIds];
            copyUpVoteIdState[copyUpVoteIdState.findIndex(val => val.id === data.id)].upvote = ++copyUpVoteIdState[copyUpVoteIdState.findIndex(val => val.id === data.id)].upvote;
            getUpvoteValues[getUpvoteValues.findIndex(val => val.id === data.id)].upvote = ++getUpvoteValues[getUpvoteValues.findIndex(val => val.id === data.id)].upvote;
            browserSessionStorage.setItem('upvote', JSON.stringify(getUpvoteValues));
            setUpvoteId(copyUpVoteIdState);
        } 
      }     
    };

    React.useEffect(() => {
      function checkUpVote() {
        if (browserSessionStorage && JSON.parse(browserSessionStorage.getItem('upvote'))  && JSON.parse(browserSessionStorage.getItem('upvote')).length > 0 && posts.length > 0) {
          posts.map(data => {
            const sotrageIndex = JSON.parse(browserSessionStorage.getItem('upvote')).findIndex(val => val.id === data.id)
            if (sotrageIndex > -1) {
              setUpvoteId(prevState => [...prevState, JSON.parse(browserSessionStorage.getItem('upvote'))[sotrageIndex]]);
            }
          })
        }
      }
      checkUpVote();
    }, [posts]);
    
    if (posts.length === 0) {
      const skeletonArray = [1,2,3,4,5,6,7];
      return (  
        <React.Fragment>
       {skeletonArray.map((post, index) => (  <div className={classes.parentSkeleton}  key={index}>
            <Skeleton variant="rect">
              <Avatar />
            </Skeleton>
            <div className={classes.childSkeletonBlock}>
            <Skeleton width="100%">
              <Typography>.</Typography>
            </Skeleton>
            <Skeleton  width="20%"/>
            </div>
      </div> ))}
      </React.Fragment>);
    }
  
    return (
      <div className={`postsWrapper ${classes.postsParnetBlock}` } >
        <div className={classes.postsBlock}>
          <List className={classes.root}>
          {posts.map((post, index) => ( <ListItem className={classes.postsLists} alignItems="flex-start" key={post.id}>
        <ListItemAvatar  className={`sqaureBoxThumb`}>
          <Avatar variant="square" className={`${classes.square}`}>{rowsPerPage*page + index}</Avatar>
        </ListItemAvatar>
        <ListItemText className={'postsListItemLink'}     
          primary={post.title} onClick={(event) => handleListItemClick(event, post, index)}/>          
            <React.Fragment>
              <Typography
                component="span"
                variant="body2"
                className={classes.inline}
                color="textPrimary"
              >
                {post.score} points by {post.by} 
                | <a onClick={(event) => upvote(event, post)}>{upvoteIds && upvoteIds.length > 0 ? upvoteIds.findIndex(data=> data.id === post.id) === -1 ? upvoteText: `${upvoteText} (${upvoteIds[upvoteIds.findIndex(data=> data.id === post.id)].upvote})`: upvoteText}</a>
                | <a onClick={(event) => hide(event, post)}>hide</a>
              </Typography>              
            </React.Fragment>
      </ListItem>))}
      </List>
      </div>
      <div className={classes.chartsBlock}>   
        {/* <LineCharts  posts={posts} page={page} rowsPerPage={rowsPerPage} upvoteIds={upvoteIds} /> */}
      </div>
      </div>
    );
  }

  export default Posts